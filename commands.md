<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">


`ps -aux` - all executing processes

`ps -auwx | grep nginx` - searches nginx from all the listing of the command.

`curl ifconfig.co` - find the external ipaddress of the server.

`curl localhost` - same as browser localhost

`chafa` - open an image in terminal.

`wikit` - search in wikipedia

`googler` - google something

`mplayer` - playing videos.
